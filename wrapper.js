class Data{
  headers = [
    'ID','Date','Account ID',
    "Account","Trans","Note",
    "Game Round",'Ccy','Delta',
    'Balance'
  ];
  raw = null;
  constructor(raw){
    this.raw = raw;
    for(var i = 0 ; i < raw.length; i++){
      this.add(i,raw[i]);
    }
  }
  add(n,info){
    this[this.headers[n]] = info;
  }
  raw(){
    return this.raw;
  }
}
class Transaction {
  tasks = [];
  results = [];
  request_detail_id = 1;
  player_id = null;

  dict = {};
  dict_ids = [];
  constructor(player_id) {
    this.player_id = player_id;
  }
  convertRaw(p) {
    let trds = (jQuery(p).find("tr"));
    let tdd_array = [];
    for (var ii = 0; ii < trds.length; ii++) {
      let tdds = jQuery(trds[ii]).find('td');
      let row = [];
      tdds.each(function(x) {
        row.push(tdds[x].textContent);
      });
      if (row.length == 0) continue;
      let d = new Data(row);
      this.dict[d.ID] = d;
      this.dict_ids.push(d.ID);
      tdd_array.push(d);
    }
    this.dict_ids = this.dict_ids.sort();
    return tdd_array;
  }
  sortedArray(){
    let ret = [];
    this.dict_ids.forEach( id => {
      ret.push(this.dict[id].raw());
    });
    return ret;
  }
  fetch(page) {
    let player = this.player_id;
    let re = jQuery.post("https://fsbo.aggregatedfun.net/ajax/player/section", {
      "playerid": player,
      "section": "transactions",
      "page": page,
      "ajax_html_ids": [
       "page",
       "header",
       "trunk",
       "search-trace-dob",
       "search-trace-phone",
       "search-trace-ip",
       "ui-datepicker-div",
       "ui-id-2",
       "ui-id-1",
       "player-" + player + "-summary-section",
       "player-" + player + "-balances-section",
       "player-" + player + "-transactions-section",
       "dp1576345583643",
       "dp1576345583644",
       "player-" + player + "-transaction-details--" + (this.request_detail_id++),
       "player-" + player + "-cashier-section",
       "player-" + player + "-cashier-request-details",
       "player-" + player + "-loyalty-section",
       "player-" + player + "-promotions-section",
       "player-" + player + "-notes-section",
       "player-" + player + "-log-section",
       "player-" + player + "-incidents-section"]

    }).then((data) => {
      let tdd_array = this.convertRaw(data.data);
      this.results = this.results.concat(tdd_array);
      return data.data;
    });
    this.tasks.push(re);
    return this;
  }
  then(f) {
    for (var i in this.tasks) {
      this.tasks[i].then(f);
    }
  }
  isActive() {
    for (var i in this.tasks) {
      if (this.tasks[i].state() == "pending") {
        return true;
      }
    }
  }
}